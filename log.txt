*****
YEAR = 1991
1 / 27
Rodando main.R para AC
2 / 27
Rodando main.R para AL
3 / 27
Rodando main.R para AM
4 / 27
Rodando main.R para AP
5 / 27
Rodando main.R para BA
6 / 27
Rodando main.R para CE
7 / 27
Rodando main.R para DF
8 / 27
Rodando main.R para ES
9 / 27
Rodando main.R para GO
10 / 27
Rodando main.R para MA
11 / 27
Rodando main.R para MG
12 / 27
Rodando main.R para MS
13 / 27
Rodando main.R para MT
14 / 27
Rodando main.R para PA
15 / 27
Rodando main.R para PB
16 / 27
Rodando main.R para PE
17 / 27
Rodando main.R para PI
18 / 27
Rodando main.R para PR
19 / 27
Rodando main.R para RJ
20 / 27
Rodando main.R para RN
21 / 27
Rodando main.R para RO
22 / 27
Rodando main.R para RR
23 / 27
Rodando main.R para RS
24 / 27
Rodando main.R para SC
25 / 27
Rodando main.R para SE
26 / 27
Rodando main.R para SP
27 / 27
Rodando main.R para TO


*****
YEAR = 1992
1 / 27
Rodando main.R para AC
2 / 27
Rodando main.R para AL
3 / 27
Rodando main.R para AM
4 / 27
Rodando main.R para AP
5 / 27
Rodando main.R para BA
6 / 27
Rodando main.R para CE
7 / 27
Rodando main.R para DF
8 / 27
Rodando main.R para ES
9 / 27
Rodando main.R para GO
10 / 27
Rodando main.R para MA
11 / 27
Rodando main.R para MG
12 / 27
Rodando main.R para MS
13 / 27
Rodando main.R para MT
14 / 27
Rodando main.R para PA
15 / 27
Rodando main.R para PB
16 / 27
Rodando main.R para PE
17 / 27
Rodando main.R para PI
18 / 27
Rodando main.R para PR
19 / 27
Rodando main.R para RJ
20 / 27
Rodando main.R para RN
21 / 27
Rodando main.R para RO
22 / 27
Rodando main.R para RR
23 / 27
Rodando main.R para RS
24 / 27
Rodando main.R para SC
25 / 27
Rodando main.R para SE
26 / 27
Rodando main.R para SP
27 / 27
Rodando main.R para TO


*****
YEAR = 1993
1 / 27
Rodando main.R para AC
2 / 27
Rodando main.R para AL
3 / 27
Rodando main.R para AM
4 / 27
Rodando main.R para AP
5 / 27
Rodando main.R para BA
6 / 27
Rodando main.R para CE
7 / 27
Rodando main.R para DF
8 / 27
Rodando main.R para ES
9 / 27
Rodando main.R para GO
10 / 27
Rodando main.R para MA
11 / 27
Rodando main.R para MG
12 / 27
Rodando main.R para MS
13 / 27
Rodando main.R para MT
14 / 27
Rodando main.R para PA
15 / 27
Rodando main.R para PB
16 / 27
Rodando main.R para PE
17 / 27
Rodando main.R para PI
18 / 27
Rodando main.R para PR
19 / 27
Rodando main.R para RJ
20 / 27
Rodando main.R para RN
21 / 27
Rodando main.R para RO
22 / 27
Rodando main.R para RR
23 / 27
Rodando main.R para RS
24 / 27
Rodando main.R para SC
25 / 27
Rodando main.R para SE
26 / 27
Rodando main.R para SP
27 / 27
Rodando main.R para TO


*****
YEAR = 1994
1 / 27
Rodando main.R para AC
2 / 27
Rodando main.R para AL
3 / 27
Rodando main.R para AM
4 / 27
Rodando main.R para AP
5 / 27
Rodando main.R para BA
6 / 27
Rodando main.R para CE
7 / 27
Rodando main.R para DF
8 / 27
Rodando main.R para ES
9 / 27
Rodando main.R para GO
10 / 27
Rodando main.R para MA
11 / 27
Rodando main.R para MG
12 / 27
Rodando main.R para MS
13 / 27
Rodando main.R para MT
14 / 27
Rodando main.R para PA
R version 3.6.1 (2019-07-05)
Platform: x86_64-conda_cos6-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /home/sibarra/anaconda3/envs/rsf/lib/R/lib/libRblas.so

locale:
 [1] LC_CTYPE=pt_BR.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=pt_BR.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=pt_BR.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=pt_BR.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=pt_BR.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] data.table_1.14.0 ggplot2_3.3.3     cptcity_1.0.6     sf_0.9-8         
[5] vein_0.9.1.3     

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.6         magrittr_2.0.1     units_0.7-1        munsell_0.5.0     
 [5] tidyselect_1.1.0   colorspace_2.0-0   R6_2.5.0           rlang_0.4.10      
 [9] fansi_0.4.2        dplyr_1.0.5        tools_3.6.1        dotCall64_1.0-1   
[13] grid_3.6.1         gtable_0.3.0       utf8_1.2.1         KernSmooth_2.23-18
[17] e1071_1.7-6        DBI_1.1.1          withr_2.4.1        ellipsis_0.3.2    
[21] class_7.3-18       tibble_3.1.0       lifecycle_1.0.0    crayon_1.4.1      
[25] purrr_0.3.4        vctrs_0.3.8        glue_1.4.2         proxy_0.4-25      
[29] compiler_3.6.1     pillar_1.5.1       scales_1.1.1       generics_0.1.0    
[33] classInt_0.4-3     pkgconfig_2.0.3   
[1] "metadata" "fleet"    "tfs"      "mileage"  "fuel"     "pmonth"   "met"     
[8] "ibge"     "notas"   
Year:  1991 
UF:      AC 
Metadata$Vehicles is:
 [1] "PC_G"          "PC_E"          "PC_FG"         "PC_FE"        
 [5] "LCV_G"         "LCV_E"         "LCV_FG"        "LCV_FE"       
 [9] "LCV_D"         "TRUCKS_SL_D"   "TRUCKS_L_D"    "TRUCKS_M_D"   
[13] "TRUCKS_SH_D"   "TRUCKS_H_D"    "BUS_URBAN_D"   "BUS_MICRO_D"  
[17] "BUS_COACH_D"   "MC_150_G"      "MC_150_500_G"  "MC_500_G"     
[21] "MC_150_FG"     "MC_150_500_FG" "MC_500_FG"     "MC_150_FE"    
[25] "MC_150_500_FE" "MC_500_FE"    
Plotting fuel 
Plotting fleet 
Plotting profiles `tfs`
Plotting mileage `tfs`

Taking some notes
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  868243 46.4    1398281  74.7  1398281 74.7
Vcells 2628020 20.1   14203248 108.4 11477569 87.6
Reading layer `AC_roads' from data source `/p1-baal/sibarra/brazil/network/present/AC_roads.gpkg' using driver `GPKG'
Simple feature collection with 1166 features and 9 fields
Geometry type: MULTILINESTRING
Dimension:     XY
Bounding box:  xmin: -73.69687 ymin: -11.12762 xmax: -66.8486 ymax: -7.381745
Geodetic CRS:  WGS 84

Names:  osm_id name highway waterway aerialway barrier man_made z_order other_tags geom 
Cleaning... 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  881111 47.1    1398281  74.7  1398281 74.7
Vcells 2647886 20.3   14203248 108.4 11477569 87.6
Plotting traffic flows
PC_G  is 0.586 of PC
PC_E  is 0.414 of PC
PC_FG is 0 of PC
PC_FE is 0 of PC
sum: 1

LCV_G  é 0.384 de LCV
LCV_E  é 0.282 de LCV
LCV_FG é 0 de LCV
LCV_FE é 0 de LCV
LCV_D  é 0.334 de LCV

sum: 1
TRUCKS_G  é 0.084 de TRUCKS
TRUCKS_FG é 0.258 de TRUCKS
TRUCKS_FE é 0.163 de TRUCKS
TRUCKS_E  é 0.235 de TRUCKS
TRUCKS_D  é 0.259 de TRUCKS

sum: 1
BUS_URBAN_D  é 0.592 de BUS
BUS_MICRO_D  é 0.131 de BUS
BUS_URBAN_D  é 0.277 de BUS
sum: 1

MC_150_G  é 0.901 de MC
MC_150_500_G  é 0.088 de MC
MC_500_G  é 0.011 de MC
MC_150_FG  é 0 de MC
MC_150_500_FG  é 0 de MC
MC_500_FG  é 0 de MC
MC_150_FE  é 0 de MC
MC_150_500_FE  é 0 de MC
MC_500_FE  é 0 de MC
sum: 1

Plotting fleet 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  999269 53.4    1848194  98.8  1398281 74.7
Vcells 2878887 22.0   14203248 108.4 11477569 87.6

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC  /p1-baal/sibarra/brazil/estimation/1991/AC/emi/*
   fuel estimation_t consumption_t estimation_consumption
1:    D  7062866 [t] 68403.792 [t]           103.2526 [1]
2:    E  6022304 [t]  8147.388 [t]           739.1699 [1]
3:    G  5740875 [t] 14260.031 [t]           402.5850 [1]

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC     fuel estimation_t consumption_t estimation_consumption
1:    D 68403.79 [t] 68403.792 [t]          1.0000000 [1]
2:    E  8147.39 [t]  8147.388 [t]          1.0000002 [1]
3:    G 14260.03 [t] 14260.031 [t]          0.9999999 [1]
Plotando EF
Estimando emissões
Plotando EF

Emissões evaporativas diurnal


Emissões evaporativas running-losses


Emissões evaporativas hot-soak

          used (Mb) gc trigger  (Mb) max used  (Mb)
Ncells 1121351 59.9    1848196  98.8  1848196  98.8
Vcells 7433050 56.8   21520375 164.2 21520375 164.2
R version 3.6.1 (2019-07-05)
Platform: x86_64-conda_cos6-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /home/sibarra/anaconda3/envs/rsf/lib/R/lib/libRblas.so

locale:
 [1] LC_CTYPE=pt_BR.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=pt_BR.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=pt_BR.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=pt_BR.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=pt_BR.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] data.table_1.14.0 ggplot2_3.3.3     cptcity_1.0.6     sf_0.9-8         
[5] vein_0.9.1.3     

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.6         magrittr_2.0.1     units_0.7-1        munsell_0.5.0     
 [5] tidyselect_1.1.0   colorspace_2.0-0   R6_2.5.0           rlang_0.4.10      
 [9] fansi_0.4.2        dplyr_1.0.5        tools_3.6.1        dotCall64_1.0-1   
[13] grid_3.6.1         gtable_0.3.0       utf8_1.2.1         KernSmooth_2.23-18
[17] e1071_1.7-6        DBI_1.1.1          withr_2.4.1        ellipsis_0.3.2    
[21] class_7.3-18       tibble_3.1.0       lifecycle_1.0.0    crayon_1.4.1      
[25] purrr_0.3.4        vctrs_0.3.8        glue_1.4.2         proxy_0.4-25      
[29] compiler_3.6.1     pillar_1.5.1       scales_1.1.1       generics_0.1.0    
[33] classInt_0.4-3     pkgconfig_2.0.3   
[1] "metadata" "fleet"    "tfs"      "mileage"  "fuel"     "pmonth"   "met"     
[8] "ibge"     "notas"   
Year:  1991 
UF:      AL 
Metadata$Vehicles is:
 [1] "PC_G"          "PC_E"          "PC_FG"         "PC_FE"        
 [5] "LCV_G"         "LCV_E"         "LCV_FG"        "LCV_FE"       
 [9] "LCV_D"         "TRUCKS_SL_D"   "TRUCKS_L_D"    "TRUCKS_M_D"   
[13] "TRUCKS_SH_D"   "TRUCKS_H_D"    "BUS_URBAN_D"   "BUS_MICRO_D"  
[17] "BUS_COACH_D"   "MC_150_G"      "MC_150_500_G"  "MC_500_G"     
[21] "MC_150_FG"     "MC_150_500_FG" "MC_500_FG"     "MC_150_FE"    
[25] "MC_150_500_FE" "MC_500_FE"    
Plotting fuel 
Plotting fleet 
Plotting profiles `tfs`
Plotting mileage `tfs`

Taking some notes
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  868243 46.4    1398281  74.7  1398281 74.7
Vcells 2628020 20.1   14203248 108.4 11477569 87.6
Reading layer `AL_roads' from data source `/p1-baal/sibarra/brazil/network/present/AL_roads.gpkg' using driver `GPKG'
Simple feature collection with 4456 features and 9 fields
Geometry type: MULTILINESTRING
Dimension:     XY
Bounding box:  xmin: -38.20963 ymin: -10.41732 xmax: -35.15383 ymax: -8.832149
Geodetic CRS:  WGS 84

Names:  osm_id name highway waterway aerialway barrier man_made z_order other_tags geom 
Cleaning... 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  881111 47.1    1398281  74.7  1398281 74.7
Vcells 2647886 20.3   14203248 108.4 11477569 87.6
Plotting traffic flows
PC_G  is 0.586 of PC
PC_E  is 0.414 of PC
PC_FG is 0 of PC
PC_FE is 0 of PC
sum: 1

LCV_G  é 0.384 de LCV
LCV_E  é 0.282 de LCV
LCV_FG é 0 de LCV
LCV_FE é 0 de LCV
LCV_D  é 0.334 de LCV

sum: 1
TRUCKS_G  é 0.084 de TRUCKS
TRUCKS_FG é 0.258 de TRUCKS
TRUCKS_FE é 0.163 de TRUCKS
TRUCKS_E  é 0.235 de TRUCKS
TRUCKS_D  é 0.259 de TRUCKS

sum: 1
BUS_URBAN_D  é 0.592 de BUS
BUS_MICRO_D  é 0.131 de BUS
BUS_URBAN_D  é 0.277 de BUS
sum: 1

MC_150_G  é 0.901 de MC
MC_150_500_G  é 0.088 de MC
MC_500_G  é 0.011 de MC
MC_150_FG  é 0 de MC
MC_150_500_FG  é 0 de MC
MC_500_FG  é 0 de MC
MC_150_FE  é 0 de MC
MC_150_500_FE  é 0 de MC
MC_500_FE  é 0 de MC
sum: 1

Plotting fleet 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  999269 53.4    1848194  98.8  1398281 74.7
Vcells 2878887 22.0   14203248 108.4 11477569 87.6

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC  /p1-baal/sibarra/brazil/estimation/1991/AL/emi/*
   fuel estimation_t consumption_t estimation_consumption
1:    D  7062866 [t] 194506.80 [t]           36.31167 [1]
2:    E  6022304 [t]  79074.24 [t]           76.16013 [1]
3:    G  5740875 [t]  57187.00 [t]          100.38776 [1]

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC     fuel  estimation_t consumption_t estimation_consumption
1:    D 194506.80 [t] 194506.80 [t]                  1 [1]
2:    E  79074.24 [t]  79074.24 [t]                  1 [1]
3:    G  57187.00 [t]  57187.00 [t]                  1 [1]
Plotando EF
Estimando emissões
Plotando EF

Emissões evaporativas diurnal


Emissões evaporativas running-losses


Emissões evaporativas hot-soak

          used (Mb) gc trigger  (Mb) max used  (Mb)
Ncells 1121351 59.9    1848194  98.8  1848194  98.8
Vcells 7433061 56.8   21698560 165.6 21698560 165.6
R version 3.6.1 (2019-07-05)
Platform: x86_64-conda_cos6-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /home/sibarra/anaconda3/envs/rsf/lib/R/lib/libRblas.so

locale:
 [1] LC_CTYPE=pt_BR.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=pt_BR.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=pt_BR.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=pt_BR.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=pt_BR.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] data.table_1.14.0 ggplot2_3.3.3     cptcity_1.0.6     sf_0.9-8         
[5] vein_0.9.1.3     

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.6         magrittr_2.0.1     units_0.7-1        munsell_0.5.0     
 [5] tidyselect_1.1.0   colorspace_2.0-0   R6_2.5.0           rlang_0.4.10      
 [9] fansi_0.4.2        dplyr_1.0.5        tools_3.6.1        dotCall64_1.0-1   
[13] grid_3.6.1         gtable_0.3.0       utf8_1.2.1         KernSmooth_2.23-18
[17] e1071_1.7-6        DBI_1.1.1          withr_2.4.1        ellipsis_0.3.2    
[21] class_7.3-18       tibble_3.1.0       lifecycle_1.0.0    crayon_1.4.1      
[25] purrr_0.3.4        vctrs_0.3.8        glue_1.4.2         proxy_0.4-25      
[29] compiler_3.6.1     pillar_1.5.1       scales_1.1.1       generics_0.1.0    
[33] classInt_0.4-3     pkgconfig_2.0.3   
[1] "metadata" "fleet"    "tfs"      "mileage"  "fuel"     "pmonth"   "met"     
[8] "ibge"     "notas"   
Year:  1991 
UF:      AM 
Metadata$Vehicles is:
 [1] "PC_G"          "PC_E"          "PC_FG"         "PC_FE"        
 [5] "LCV_G"         "LCV_E"         "LCV_FG"        "LCV_FE"       
 [9] "LCV_D"         "TRUCKS_SL_D"   "TRUCKS_L_D"    "TRUCKS_M_D"   
[13] "TRUCKS_SH_D"   "TRUCKS_H_D"    "BUS_URBAN_D"   "BUS_MICRO_D"  
[17] "BUS_COACH_D"   "MC_150_G"      "MC_150_500_G"  "MC_500_G"     
[21] "MC_150_FG"     "MC_150_500_FG" "MC_500_FG"     "MC_150_FE"    
[25] "MC_150_500_FE" "MC_500_FE"    
Plotting fuel 
Plotting fleet 
Plotting profiles `tfs`
Plotting mileage `tfs`

Taking some notes
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  868243 46.4    1398281  74.7  1398281 74.7
Vcells 2628020 20.1   14203248 108.4 11477569 87.6
Reading layer `AM_roads' from data source `/p1-baal/sibarra/brazil/network/present/AM_roads.gpkg' using driver `GPKG'
Simple feature collection with 2696 features and 9 fields
Geometry type: MULTILINESTRING
Dimension:     XY
Bounding box:  xmin: -70.19253 ymin: -8.991085 xmax: -56.66388 ymax: 1.185068
Geodetic CRS:  WGS 84

Names:  osm_id name highway waterway aerialway barrier man_made z_order other_tags geom 
Cleaning... 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  881111 47.1    1398281  74.7  1398281 74.7
Vcells 2647886 20.3   14203248 108.4 11477569 87.6
Plotting traffic flows
PC_G  is 0.586 of PC
PC_E  is 0.414 of PC
PC_FG is 0 of PC
PC_FE is 0 of PC
sum: 1

LCV_G  é 0.384 de LCV
LCV_E  é 0.282 de LCV
LCV_FG é 0 de LCV
LCV_FE é 0 de LCV
LCV_D  é 0.334 de LCV

sum: 1
TRUCKS_G  é 0.084 de TRUCKS
TRUCKS_FG é 0.258 de TRUCKS
TRUCKS_FE é 0.163 de TRUCKS
TRUCKS_E  é 0.235 de TRUCKS
TRUCKS_D  é 0.259 de TRUCKS

sum: 1
BUS_URBAN_D  é 0.592 de BUS
BUS_MICRO_D  é 0.131 de BUS
BUS_URBAN_D  é 0.277 de BUS
sum: 1

MC_150_G  é 0.901 de MC
MC_150_500_G  é 0.088 de MC
MC_500_G  é 0.011 de MC
MC_150_FG  é 0 de MC
MC_150_500_FG  é 0 de MC
MC_500_FG  é 0 de MC
MC_150_FE  é 0 de MC
MC_150_500_FE  é 0 de MC
MC_500_FE  é 0 de MC
sum: 1

Plotting fleet 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  999269 53.4    1848194  98.8  1398281 74.7
Vcells 2878887 22.0   14203248 108.4 11477569 87.6

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC  /p1-baal/sibarra/brazil/estimation/1991/AM/emi/*
   fuel estimation_t consumption_t estimation_consumption
1:    D  7062866 [t] 277332.88 [t]           25.46711 [1]
2:    E  6022304 [t]  55035.36 [t]          109.42608 [1]
3:    G  5740875 [t]  84792.67 [t]           67.70484 [1]

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC     fuel  estimation_t consumption_t estimation_consumption
1:    D 277332.88 [t] 277332.88 [t]                  1 [1]
2:    E  55035.36 [t]  55035.36 [t]                  1 [1]
3:    G  84792.67 [t]  84792.67 [t]                  1 [1]
Plotando EF
Estimando emissões
Plotando EF
R version 3.6.1 (2019-07-05)
Platform: x86_64-conda_cos6-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /home/sibarra/anaconda3/envs/rsf/lib/R/lib/libRblas.so

locale:
 [1] LC_CTYPE=pt_BR.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=pt_BR.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=pt_BR.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=pt_BR.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=pt_BR.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] data.table_1.14.0 ggplot2_3.3.3     cptcity_1.0.6     sf_0.9-8         
[5] vein_0.9.1.3     

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.6         magrittr_2.0.1     units_0.7-1        munsell_0.5.0     
 [5] tidyselect_1.1.0   colorspace_2.0-0   R6_2.5.0           rlang_0.4.10      
 [9] fansi_0.4.2        dplyr_1.0.5        tools_3.6.1        dotCall64_1.0-1   
[13] grid_3.6.1         gtable_0.3.0       utf8_1.2.1         KernSmooth_2.23-18
[17] e1071_1.7-6        DBI_1.1.1          withr_2.4.1        ellipsis_0.3.2    
[21] class_7.3-18       tibble_3.1.0       lifecycle_1.0.0    crayon_1.4.1      
[25] purrr_0.3.4        vctrs_0.3.8        glue_1.4.2         proxy_0.4-25      
[29] compiler_3.6.1     pillar_1.5.1       scales_1.1.1       generics_0.1.0    
[33] classInt_0.4-3     pkgconfig_2.0.3   
[1] "metadata" "fleet"    "tfs"      "mileage"  "fuel"     "pmonth"   "met"     
[8] "ibge"     "notas"   
Year:  1991 
UF:      AC 
Metadata$Vehicles is:
 [1] "PC_G"          "PC_E"          "PC_FG"         "PC_FE"        
 [5] "LCV_G"         "LCV_E"         "LCV_FG"        "LCV_FE"       
 [9] "LCV_D"         "TRUCKS_SL_D"   "TRUCKS_L_D"    "TRUCKS_M_D"   
[13] "TRUCKS_SH_D"   "TRUCKS_H_D"    "BUS_URBAN_D"   "BUS_MICRO_D"  
[17] "BUS_COACH_D"   "MC_150_G"      "MC_150_500_G"  "MC_500_G"     
[21] "MC_150_FG"     "MC_150_500_FG" "MC_500_FG"     "MC_150_FE"    
[25] "MC_150_500_FE" "MC_500_FE"    
Plotting fuel 
Plotting fleet 
Plotting profiles `tfs`
Plotting mileage `tfs`

Taking some notes
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  868243 46.4    1398281  74.7  1398281 74.7
Vcells 2628020 20.1   14203248 108.4 11477569 87.6
Reading layer `AC_roads' from data source `/p1-baal/sibarra/brazil/network/present/AC_roads.gpkg' using driver `GPKG'
Simple feature collection with 1166 features and 9 fields
Geometry type: MULTILINESTRING
Dimension:     XY
Bounding box:  xmin: -73.69687 ymin: -11.12762 xmax: -66.8486 ymax: -7.381745
Geodetic CRS:  WGS 84

Names:  osm_id name highway waterway aerialway barrier man_made z_order other_tags geom 
Cleaning... 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  881111 47.1    1398281  74.7  1398281 74.7
Vcells 2647886 20.3   14203248 108.4 11477569 87.6
Plotting traffic flows
PC_G  is 0.586 of PC
PC_E  is 0.414 of PC
PC_FG is 0 of PC
PC_FE is 0 of PC
sum: 1

LCV_G  é 0.384 de LCV
LCV_E  é 0.282 de LCV
LCV_FG é 0 de LCV
LCV_FE é 0 de LCV
LCV_D  é 0.334 de LCV

sum: 1
TRUCKS_G  é 0.084 de TRUCKS
TRUCKS_FG é 0.258 de TRUCKS
TRUCKS_FE é 0.163 de TRUCKS
TRUCKS_E  é 0.235 de TRUCKS
TRUCKS_D  é 0.259 de TRUCKS

sum: 1
BUS_URBAN_D  é 0.592 de BUS
BUS_MICRO_D  é 0.131 de BUS
BUS_URBAN_D  é 0.277 de BUS
sum: 1

MC_150_G  é 0.901 de MC
MC_150_500_G  é 0.088 de MC
MC_500_G  é 0.011 de MC
MC_150_FG  é 0 de MC
MC_150_500_FG  é 0 de MC
MC_500_FG  é 0 de MC
MC_150_FE  é 0 de MC
MC_150_500_FE  é 0 de MC
MC_500_FE  é 0 de MC
sum: 1

Plotting fleet 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  999269 53.4    1848194  98.8  1398281 74.7
Vcells 2878887 22.0   14203248 108.4 11477569 87.6

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC  /p1-baal/sibarra/brazil/estimation/1991/AC/emi/*
   fuel estimation_t consumption_t estimation_consumption
1:    D  7062866 [t] 68403.792 [t]           103.2526 [1]
2:    E  6022304 [t]  8147.388 [t]           739.1699 [1]
3:    G  5740875 [t] 14260.031 [t]           402.5850 [1]

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC     fuel estimation_t consumption_t estimation_consumption
1:    D 68403.79 [t] 68403.792 [t]          1.0000000 [1]
2:    E  8147.39 [t]  8147.388 [t]          1.0000002 [1]
3:    G 14260.03 [t] 14260.031 [t]          0.9999999 [1]
Plotando EF
Estimando emissões
Plotando EF

Emissões evaporativas diurnal


Emissões evaporativas running-losses


Emissões evaporativas hot-soak

          used (Mb) gc trigger  (Mb) max used  (Mb)
Ncells 1121351 59.9    1848194  98.8  1848194  98.8
Vcells 7433050 56.8   21520371 164.2 21520371 164.2

 PC_G          
 PC_E          
 PC_FG         
 PC_FE         
 LCV_G         
 LCV_E         
 LCV_FG        
 LCV_FE        
 LCV_D         
 TRUCKS_SL_D   
 TRUCKS_L_D    
 TRUCKS_M_D    
 TRUCKS_SH_D   
 TRUCKS_H_D    
 BUS_URBAN_D   
 BUS_MICRO_D   
 BUS_COACH_D   
 MC_150_G      
 MC_150_500_G  
 MC_500_G      
 MC_150_FG     
 MC_150_500_FG 
 MC_500_FG     
 MC_150_FE     
 MC_150_500_FE 
 MC_500_FE     R version 3.6.1 (2019-07-05)
Platform: x86_64-conda_cos6-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /home/sibarra/anaconda3/envs/rsf/lib/R/lib/libRblas.so

locale:
 [1] LC_CTYPE=pt_BR.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=pt_BR.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=pt_BR.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=pt_BR.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=pt_BR.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] data.table_1.14.0 ggplot2_3.3.3     cptcity_1.0.6     sf_0.9-8         
[5] vein_0.9.1.3     

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.6         magrittr_2.0.1     units_0.7-1        munsell_0.5.0     
 [5] tidyselect_1.1.0   colorspace_2.0-0   R6_2.5.0           rlang_0.4.10      
 [9] fansi_0.4.2        dplyr_1.0.5        tools_3.6.1        dotCall64_1.0-1   
[13] grid_3.6.1         gtable_0.3.0       utf8_1.2.1         KernSmooth_2.23-18
[17] e1071_1.7-6        DBI_1.1.1          withr_2.4.1        ellipsis_0.3.2    
[21] class_7.3-18       tibble_3.1.0       lifecycle_1.0.0    crayon_1.4.1      
[25] purrr_0.3.4        vctrs_0.3.8        glue_1.4.2         proxy_0.4-25      
[29] compiler_3.6.1     pillar_1.5.1       scales_1.1.1       generics_0.1.0    
[33] classInt_0.4-3     pkgconfig_2.0.3   
[1] "metadata" "fleet"    "tfs"      "mileage"  "fuel"     "pmonth"   "met"     
[8] "ibge"     "notas"   
Year:  1991 
UF:      AL 
Metadata$Vehicles is:
 [1] "PC_G"          "PC_E"          "PC_FG"         "PC_FE"        
 [5] "LCV_G"         "LCV_E"         "LCV_FG"        "LCV_FE"       
 [9] "LCV_D"         "TRUCKS_SL_D"   "TRUCKS_L_D"    "TRUCKS_M_D"   
[13] "TRUCKS_SH_D"   "TRUCKS_H_D"    "BUS_URBAN_D"   "BUS_MICRO_D"  
[17] "BUS_COACH_D"   "MC_150_G"      "MC_150_500_G"  "MC_500_G"     
[21] "MC_150_FG"     "MC_150_500_FG" "MC_500_FG"     "MC_150_FE"    
[25] "MC_150_500_FE" "MC_500_FE"    
Plotting fuel 
Plotting fleet 
Plotting profiles `tfs`
Plotting mileage `tfs`

Taking some notes
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  868243 46.4    1398281  74.7  1398281 74.7
Vcells 2628020 20.1   14203248 108.4 11477569 87.6
Reading layer `AL_roads' from data source `/p1-baal/sibarra/brazil/network/present/AL_roads.gpkg' using driver `GPKG'
Simple feature collection with 4456 features and 9 fields
Geometry type: MULTILINESTRING
Dimension:     XY
Bounding box:  xmin: -38.20963 ymin: -10.41732 xmax: -35.15383 ymax: -8.832149
Geodetic CRS:  WGS 84

Names:  osm_id name highway waterway aerialway barrier man_made z_order other_tags geom 
Cleaning... 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  881111 47.1    1398281  74.7  1398281 74.7
Vcells 2647886 20.3   14203248 108.4 11477569 87.6
Plotting traffic flows
PC_G  is 0.586 of PC
PC_E  is 0.414 of PC
PC_FG is 0 of PC
PC_FE is 0 of PC
sum: 1

LCV_G  é 0.384 de LCV
LCV_E  é 0.282 de LCV
LCV_FG é 0 de LCV
LCV_FE é 0 de LCV
LCV_D  é 0.334 de LCV

sum: 1
TRUCKS_G  é 0.084 de TRUCKS
TRUCKS_FG é 0.258 de TRUCKS
TRUCKS_FE é 0.163 de TRUCKS
TRUCKS_E  é 0.235 de TRUCKS
TRUCKS_D  é 0.259 de TRUCKS

sum: 1
BUS_URBAN_D  é 0.592 de BUS
BUS_MICRO_D  é 0.131 de BUS
BUS_URBAN_D  é 0.277 de BUS
sum: 1

MC_150_G  é 0.901 de MC
MC_150_500_G  é 0.088 de MC
MC_500_G  é 0.011 de MC
MC_150_FG  é 0 de MC
MC_150_500_FG  é 0 de MC
MC_500_FG  é 0 de MC
MC_150_FE  é 0 de MC
MC_150_500_FE  é 0 de MC
MC_500_FE  é 0 de MC
sum: 1

Plotting fleet 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  999269 53.4    1848194  98.8  1398281 74.7
Vcells 2878887 22.0   14203248 108.4 11477569 87.6

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC  /p1-baal/sibarra/brazil/estimation/1991/AL/emi/*
   fuel estimation_t consumption_t estimation_consumption
1:    D  7062866 [t] 194506.80 [t]           36.31167 [1]
2:    E  6022304 [t]  79074.24 [t]           76.16013 [1]
3:    G  5740875 [t]  57187.00 [t]          100.38776 [1]

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC     fuel  estimation_t consumption_t estimation_consumption
1:    D 194506.80 [t] 194506.80 [t]                  1 [1]
2:    E  79074.24 [t]  79074.24 [t]                  1 [1]
3:    G  57187.00 [t]  57187.00 [t]                  1 [1]
Plotando EF
Estimando emissões
Plotando EF

Emissões evaporativas diurnal
R version 3.6.1 (2019-07-05)
Platform: x86_64-conda_cos6-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /home/sibarra/anaconda3/envs/rsf/lib/R/lib/libRblas.so

locale:
 [1] LC_CTYPE=pt_BR.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=pt_BR.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=pt_BR.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=pt_BR.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=pt_BR.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] data.table_1.14.0 ggplot2_3.3.3     cptcity_1.0.6     sf_0.9-8         
[5] vein_0.9.1.3     

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.6         magrittr_2.0.1     units_0.7-1        munsell_0.5.0     
 [5] tidyselect_1.1.0   colorspace_2.0-0   R6_2.5.0           rlang_0.4.10      
 [9] fansi_0.4.2        dplyr_1.0.5        tools_3.6.1        dotCall64_1.0-1   
[13] grid_3.6.1         gtable_0.3.0       utf8_1.2.1         KernSmooth_2.23-18
[17] e1071_1.7-6        DBI_1.1.1          withr_2.4.1        ellipsis_0.3.2    
[21] class_7.3-18       tibble_3.1.0       lifecycle_1.0.0    crayon_1.4.1      
[25] purrr_0.3.4        vctrs_0.3.8        glue_1.4.2         proxy_0.4-25      
[29] compiler_3.6.1     pillar_1.5.1       scales_1.1.1       generics_0.1.0    
[33] classInt_0.4-3     pkgconfig_2.0.3   
[1] "metadata" "fleet"    "tfs"      "mileage"  "fuel"     "pmonth"   "met"     
[8] "ibge"     "notas"   
Year:  1991 
UF:      AC 
Metadata$Vehicles is:
 [1] "PC_G"          "PC_E"          "PC_FG"         "PC_FE"        
 [5] "LCV_G"         "LCV_E"         "LCV_FG"        "LCV_FE"       
 [9] "LCV_D"         "TRUCKS_SL_D"   "TRUCKS_L_D"    "TRUCKS_M_D"   
[13] "TRUCKS_SH_D"   "TRUCKS_H_D"    "BUS_URBAN_D"   "BUS_MICRO_D"  
[17] "BUS_COACH_D"   "MC_150_G"      "MC_150_500_G"  "MC_500_G"     
[21] "MC_150_FG"     "MC_150_500_FG" "MC_500_FG"     "MC_150_FE"    
[25] "MC_150_500_FE" "MC_500_FE"    
Plotting fuel 
Plotting fleet 
Plotting profiles `tfs`
Plotting mileage `tfs`

Taking some notes
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  868243 46.4    1398281  74.7  1398281 74.7
Vcells 2628020 20.1   14203248 108.4 11477569 87.6
Reading layer `AC_roads' from data source `/p1-baal/sibarra/brazil/network/present/AC_roads.gpkg' using driver `GPKG'
Simple feature collection with 1166 features and 9 fields
Geometry type: MULTILINESTRING
Dimension:     XY
Bounding box:  xmin: -73.69687 ymin: -11.12762 xmax: -66.8486 ymax: -7.381745
Geodetic CRS:  WGS 84

Names:  osm_id name highway waterway aerialway barrier man_made z_order other_tags geom 
Cleaning... 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  881111 47.1    1398281  74.7  1398281 74.7
Vcells 2647886 20.3   14203248 108.4 11477569 87.6
Plotting traffic flows
PC_G  is 0.586 of PC
PC_E  is 0.414 of PC
PC_FG is 0 of PC
PC_FE is 0 of PC
sum: 1

LCV_G  é 0.384 de LCV
LCV_E  é 0.282 de LCV
LCV_FG é 0 de LCV
LCV_FE é 0 de LCV
LCV_D  é 0.334 de LCV

sum: 1
TRUCKS_G  é 0.084 de TRUCKS
TRUCKS_FG é 0.258 de TRUCKS
TRUCKS_FE é 0.163 de TRUCKS
TRUCKS_E  é 0.235 de TRUCKS
TRUCKS_D  é 0.259 de TRUCKS

sum: 1
BUS_URBAN_D  é 0.592 de BUS
BUS_MICRO_D  é 0.131 de BUS
BUS_URBAN_D  é 0.277 de BUS
sum: 1

MC_150_G  é 0.901 de MC
MC_150_500_G  é 0.088 de MC
MC_500_G  é 0.011 de MC
MC_150_FG  é 0 de MC
MC_150_500_FG  é 0 de MC
MC_500_FG  é 0 de MC
MC_150_FE  é 0 de MC
MC_150_500_FE  é 0 de MC
MC_500_FE  é 0 de MC
sum: 1

Plotting fleet 
          used (Mb) gc trigger  (Mb) max used (Mb)
Ncells  999269 53.4    1848194  98.8  1398281 74.7
Vcells 2878887 22.0   14203248 108.4 11477569 87.6

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC  /p1-baal/sibarra/brazil/estimation/1991/AC/emi/*
   fuel estimation_t consumption_t estimation_consumption
1:    D  7062866 [t] 68403.792 [t]           103.2526 [1]
2:    E  6022304 [t]  8147.388 [t]           739.1699 [1]
3:    G  5740875 [t] 14260.031 [t]           402.5850 [1]

 PC_G          FC  
 PC_E          FC  
 PC_FG         FC  
 PC_FE         FC  
 LCV_G         FC  
 LCV_E         FC  
 LCV_FG        FC  
 LCV_FE        FC  
 LCV_D         FC  
 TRUCKS_SL_D   FC  
 TRUCKS_L_D    FC  
 TRUCKS_M_D    FC  
 TRUCKS_SH_D   FC  
 TRUCKS_H_D    FC  
 BUS_URBAN_D   FC  
 BUS_MICRO_D   FC  
 BUS_COACH_D   FC  
 MC_150_G      FC  
 MC_150_500_G  FC  
 MC_500_G      FC  
 MC_150_FG     FC  
 MC_150_500_FG FC  
 MC_500_FG     FC  
 MC_150_FE     FC  
 MC_150_500_FE FC  
 MC_500_FE     FC     fuel estimation_t consumption_t estimation_consumption
1:    D 68403.79 [t] 68403.792 [t]          1.0000000 [1]
2:    E  8147.39 [t]  8147.388 [t]          1.0000002 [1]
3:    G 14260.03 [t] 14260.031 [t]          0.9999999 [1]
Plotando EF
Estimando emissões
Plotando EF

Emissões evaporativas diurnal


Emissões evaporativas running-losses


Emissões evaporativas hot-soak

          used (Mb) gc trigger  (Mb) max used  (Mb)
Ncells 1121351 59.9    1848194  98.8  1848194  98.8
Vcells 7433050 56.8   21520371 164.2 21520371 164.2

 PC_G          
 PC_E          
 PC_FG         
 PC_FE         
 LCV_G         
 LCV_E         
 LCV_FG        
 LCV_FE        
 LCV_D         
 TRUCKS_SL_D   
 TRUCKS_L_D    
 TRUCKS_M_D    
 TRUCKS_SH_D   
 TRUCKS_H_D    
 BUS_URBAN_D   
 BUS_MICRO_D   
 BUS_COACH_D   
 MC_150_G      
 MC_150_500_G  
 MC_500_G      
 MC_150_FG     
 MC_150_500_FG 
 MC_500_FG     
 MC_150_FE     
 MC_150_500_FE 
 MC_500_FE               used (Mb) gc trigger  (Mb) max used  (Mb)
Ncells 1122704 60.0    1848194  98.8  1848194  98.8
Vcells 7967642 60.8   21520371 164.2 21520371 164.2
Distribuyendo las emisiones en las calles
Cortando las calles para grilla
Distribuyendo las emisiones en las grilla
Sum of street emissions 291363019239.77
Sum of gridded emissions 291363019239.77
Guardando emisiones
    pollutant            V1
 1:        CO  20778.44 [t]
 2:        HC   2463.12 [t]
 3:      NMHC   3090.99 [t]
 4:       NOx   3501.17 [t]
 5:       CO2 257562.67 [t]
 6:        PM    179.66 [t]
 7:       NO2    334.14 [t]
 8:        NO   2678.22 [t]
 9:       SO2      7.13 [t]
10:       CH4    570.77 [t]
11:       NH3      1.53 [t]
12:       N2O     10.88 [t]
13:   PM25RES     59.31 [t]
14:   PM10RES    125.00 [t]
          used (Mb) gc trigger  (Mb) max used  (Mb)
Ncells 1131085 60.5    2580580 137.9  1942256 103.8
Vcells 8012408 61.2   47422373 361.9 49330142 376.4

Plotando calles

Plotando categorias por total

Plotando categorias por type_emi

Plotando categorias por total y tipo

Plotando categorias por mes

Plotando grilla
          used (Mb) gc trigger  (Mb) max used  (Mb)
Ncells 1146939 61.3    2580580 137.9  2580580 137.9
Vcells 8017205 61.2   30350320 231.6 49330142 376.4
01  02  03  04  05  06  07  08  09  10  11  12             used (Mb) gc trigger  (Mb)  max used   (Mb)
Ncells  1153486 61.7    7289844 389.4  11011741  588.1
Vcells 12236455 93.4  112782536 860.5 145276050 1108.4
01  02  03  04  05  06  07  08  09  10  11  12             used (Mb) gc trigger  (Mb)  max used   (Mb)
Ncells  1153431 61.6    3352185 179.1  11011741  588.1
Vcells 12213510 93.2   83220308 635.0 145276050 1108.4
01  02  03  04  05  06  07  08  09  10  11  12             used (Mb) gc trigger  (Mb)  max used   (Mb)
Ncells  1153501 61.7    3843273 205.3  11011741  588.1
Vcells 12242698 93.5   87061735 664.3 145276050 1108.4
01  02  03  04  05  06  